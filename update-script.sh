#!/bin/sh
set -e
DEFAULTDIR="../var/etc-default"
USERDIR="../var/etc-user"
cd "$(dirname "$0")"
exec 3>&1
ALLUNKNOWN=""
ALLMODIFIED=""
ALLDELETED=""
IFS="$(printf '\nx')"
# HACK: there is no way to have trailing newline included in the string output 
# by $() command and #!/bin/sh does not support $''.
IFS="${IFS%x}"
STATUSES="$(printf '%s\n' unknown modified deleted removed)"
# Warning: #!/bin/sh (specifically dash) does not support ${//}
STATUSES_HELP="$(printf '%s|' unknown modified deleted)removed"
REVERT=""
FINISH=""
MSG=""
SUMMARY=""
FORCE_UPDATE_METADATA=""

REVERT=""
trap 'eval "$REVERT"' EXIT

while test "$#" -ne 0 ; do
    if test "x${1#--}" = "x${1}" ; then
        break
    fi
    for st in $STATUSES ; do
        if test "x$1" = "x--all"$st ; then
            eval ALL_"$st"=yes
            shift
            break
        fi
        if test "x$1" = "x--no"$st ; then
            eval ALL_"$st"=no
            shift
            break
        fi
    done
    if test "x$1" = "x--all" ; then
        for st in $STATUSES ; do
            eval ALL_"$st"=yes
        done
        shift
    fi
    if test "x$1" = "x--none" ; then
        for st in $STATUSES ; do
            eval ALL_"$st"=no
        done
        shift
    fi
    if test "x$1" = "x--" ; then
        shift
        break
    fi
    if test "x$1" = "x--help" ; then
        echo "Usage: $0 --(all|none) --(all|no)(${STATUSES_HELP})" \
            "--update-metadata [--] files"
        exit
    fi
    if test "x$1" = "x--update-metadata" ; then
        FORCE_UPDATE_METADATA=1
        shift
    fi
done

update_metadata ()
{
    local file_desc_cmd='
        printf "%sperm:%s${s}uid:%s${s}gid:%s${e}" \
            "$(printf "%s%s" "$file" "$f" \
               | perl -p -e \
                   "s/[\0-\x1F\\\\]/sprintf \"\\\\x%02X\", ord(\$&)/ge")" \
            $(stat "$file" -c "%a %u %g") \
    '
    local sh_script="
        for file ; do
            $file_desc_cmd
        done
    "
    local f=':'
    local s=';'
    local e='\n'
    find . -path ./.hg -prune -o \( \
        \! -user root -o \! -group root \
        -o \( -type f -a \! -perm 0644 \) \
        -o \( -type d -a \! -perm 0755 \) \) \
        -print0 \
        \
        | env f="${f}" s="${s}" e="${e}" xargs -0 sh -c "$sh_script" - \
        | sort > "$DEFAULTDIR/.metadata"

    hg add -R "$DEFAULTDIR" "$DEFAULTDIR/.metadata"
    hg commit -R "$DEFAULTDIR" "$DEFAULTDIR/.metadata" -m "Update metadata" \
        || true
}

init_dirs ()
{
    if ! test -d "$DEFAULTDIR" ; then
        mkdir -p "$(dirname "$DEFAULTDIR")"
        hg clone -r default . "$DEFAULTDIR"
    else
        hg push --new-branch -r default "$DEFAULTDIR" || true
    fi
    if ! test -d "$USERDIR" ; then
        mkdir -p "$(dirname "$USERDIR")"
        hg clone . "$USERDIR"
    else
        hg push --new-branch -r user "$USERDIR" || true
    fi
    hg update -R "$DEFAULTDIR" -r default
    hg update -R "$USERDIR" -r user
}

init_dirs

atfinish ()
{
    local cmd="$1"
    FINISH="${FINISH}
    $cmd"
}
atrevert ()
{
    local cmd="$1"
    REVERT="${REVERT}
    $cmd"
}
message ()
{
    local status="$1"
    local file="$2"
    SUMMARY="$SUMMARY$status"
    MSG="$(printf "%s\n%s %s" "${MSG}" "$status" "$file")"
}

esc ()
{
    local f="$1"
    echo -n \'
    echo -n "$1" | sed "s/'/'\\\\''/g"
    echo -n \'
}
reg_esc ()
{
    local f="$1"
    echo "$f" | sed 's/[^a-zA-Z0-9]/\\\0/g'
}

has_choice ()
{
    local choices="$1"
    local choice="$2"
    if test "${#choice}" -ne 1 ; then
        return 1
    else
        while test "x$choices" != "x" ; do
            if test "x${choices##${choice}*}" = "x" ; then
                return 0
            fi
            choices="${choices#?}"
        done
        return 1
    fi
}
input ()
{
    local prompt="$1"
    local choices="$2"
    local default="$3"
    local choice
    echo -n "$prompt [$choices]? [$default] "
    while read choice ; do
        if test -z "$choice" ; then
            input="$default"
            break
        elif has_choice "$choices" "$choice" ; then
            input="$choice"
            break
        fi
        echo -n "$prompt [$choices]? [$default] "
    done
}

vcs_def_cmd_str_file ()
{
    local cmd="$1"
    local file="$2"
    echo "$1 -R $(esc "$DEFAULTDIR") $(esc "$DEFAULTDIR/$file")"
}

vcs_def_add ()
{
    local tgt="$1"
    hg add -R "$DEFAULTDIR" "$DEFAULTDIR/$tgt" && \
        atrevert "$(vcs_def_cmd_str_file "hg rm -f" "$tgt")"
}
vcs_def_mod ()
{
    local tgt="$1"
    atrevert "$(vcs_def_cmd_str_file "hg revert --no-backup" "$tgt")"
}
vcs_def_patch ()
{
    local fname="$1"
    if ! test -L "$fname" && test -f "$fname" ; then
        if hg diff --text "$fname" | \
            patch --directory="$DEFAULTDIR" \
                  --unified \
                  --strip=1 \
                  --force \
                  --forward \
                  --silent \
                  --no-backup-if-mismatch \
                  --reject-file=/dev/stderr
        then
            vcs_def_mod "$fname"
        else
            return 1
        fi
    else
        move_modified "$fname" "$DEFAULTDIR/$fname"
    fi
}
vcs_def_remove ()
{
    hg remove -R "$DEFAULTDIR" "$DEFAULTDIR/$fname"
    vcs_def_mod "$fname"
    atfinish "hg revert -- $(esc "$fname")"
}

ignore ()
{
    local fname="$1"
    local pattern="^$(reg_esc "$fname")\$"
    message I "$fname"
    echo "$pattern" >> "$DEFAULTDIR/.hgignore"
    local eept="$(esc "$(reg_esc "$pattern")")"
    atrevert "sed -r -i -e '/^'$eept'$/d' $(esc "$DEFAULTDIR/.hgignore")"
}
move_file_to_file ()
{
    local src="$1"
    local tgt="$2"
    if ! test -d "$(dirname "$tgt")" ; then
        echo "Directory $(dirname "$tgt") does not exist." >&3
        return 1
    fi
    cp --no-target-directory --no-dereference --preserve=all "$src" "$tgt"
    atfinish "rm $(esc "$src")"
    atrevert "rm $(esc "$tgt")"
}
move_modified ()
{
    local src="$1"
    local tgt="$2"
    move_file_to_file "$src" "$tgt"
    atfinish "hg revert --no-backup '$(echo "${src}")'"
    vcs_def_mod "$src"
}

handle_unknown ()
{
    local fname="$1"
    local include="$2"
    if test "x$include" != "xyes" ; then
        printf "\e[32m%s\e[0m\n" "$fname" >&3
        cat "$fname" >&3
        grep -wF NOT < "$fname" >&2 || true
        local input
        input "Add file" "yni" "y" >&3 </dev/tty
        if test "x$input" = "xn" ; then
            return
        elif test "x$input" = "xi" ; then
            ignore "$fname"
            return
        fi
    fi
    local dir="$(dirname "$fname")"
    test -d "$DEFAULTDIR/$dir" || mkdir -p "$DEFAULTDIR/$dir"
    if echo "$fname" | grep '\(/\|^\)\._cfg[0-9]\+_[^/]\+$' &>/dev/null
    then
        local target="$(echo "$fname" | sed 's@\._cfg[0-9]\+_\([^/]\+\)$@\1@')"
        move_file_to_file "$fname" "$DEFAULTDIR/$target"
        if test -n "$(hg status -R "$DEFAULTDIR" -rdui "$DEFAULTDIR/$target")"
        then
            vcs_def_add "$target" >&3
            message A "$target"
        elif test -n "$(hg status -R "$DEFAULTDIR" -m "$DEFAULTDIR/$target")"
        then
            vcs_def_mod "$target" >&3
            message C "$target"
        fi
    else
        move_file_to_file "$fname" "$DEFAULTDIR/$fname"
        vcs_def_add "$fname" >&3
        message N "$fname"
    fi
}
handle_modified ()
{
    local fname="$1"
    local include="$2"
    local stchar="$(hg status --rev user --rev default "$fname")"
    stchar="${stchar% *}"
    if test "x$stchar" = "xM" || test "x$stchar" = "xA" ; then
        if test "x$include" != "xyes" ; then
            printf "\e[32m%s\e[0m\n" "$fname" >&3
            hg diff --color=always "$fname" >&3
            local input
            input "Add file" "yni" "y" >&3 </dev/tty
            if test "x$input" = "xn" ; then
                return
            elif test "x$input" = "xi" ; then
                ignore "$fname"
                return
            fi
        fi
        move_modified "$fname" "$DEFAULTDIR/$fname"
        message M "$fname"
    elif test "x$stchar" = "xR" ; then
        if test "x$include" != "xyes" ; then
            printf "\e[32m%s\e[0m\n" "$fname" >&3
            cat "$fname" >&3
            local input
            input "Add file modified in user" "yni" "y" >&3 </dev/tty
            if test "x$input" = "xn" ; then
                return
            elif test "x$input" = "xi" ; then
                ignore "$fname"
                return
            fi
        fi
        move_modified "$fname" "$DEFAULTDIR/$fname"
        vcs_def_add "$target"
        message M "$fname"
    else
        if test "x$include" = "xyes" ; then
            vcs_def_patch "$fname"
            message M "$fname"
        fi
    fi
}
file_exists ()
{
    # Check that file exists, including existing as a broken symbolic link
    local fname="$1"
    if test -e "$fname" || test -h "$fname" ; then
        return 0
    else
        return 1
    fi
}
handle_deleted ()
{
    local fname="$1"
    local include="$2"
    if file_exists "$DEFAULTDIR/$fname" ; then
        if test "x$include" != "xyes" ; then
            printf "\e[32m%s\e[0m\n" "$fname" >&3
            local input
            input "Remove file" "yn" "y" >&3 </dev/tty
            if test "x$input" = "xn" ; then
                return
            fi
        fi
        vcs_def_remove "$fname"
        message R "$fname"
    fi
}
handle_removed ()
{
    handle_deleted "$@"
}

if test $# -ge 1 ; then
    for st in $STATUSES ; do
        repo_statuses="$(hg status "--$st" -- "$@" | sed 's/^..//')"
        for fname in $repo_statuses ; do
            handle_"$st" "$fname" "yes"
        done
    done
fi
for st in $STATUSES ; do
    eval include='$ALL_'$st
    if test "x$include" = "xno" ; then
        continue
    fi
    repo_statuses="$(hg status "--$st" | sed 's/^..//')"
    for fname in $repo_statuses ; do
        handle_"$st" "$fname" "$include"
    done
done
exec 3>&-
test -z "$MSG$FORCE_UPDATE_METADATA" && exit 1

update_metadata

if test -n "$MSG" ; then
    MSG="$(printf "%s\n\n%s" "$SUMMARY" "$MSG")"
    hg commit -R "$DEFAULTDIR" -m "$MSG"
fi

hg pull "$DEFAULTDIR"                        &&
hg push "$USERDIR"                           &&
hg -R "$USERDIR" update -C user              &&
hg -R "$USERDIR" merge -r default -t vimdiff &&
hg -R "$USERDIR" commit -m Merge             &&
eval "$FINISH"                               &&
hg pull -u "$USERDIR"                        ||
exit 4
REVERT=""
