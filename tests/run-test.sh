#!/bin/sh
TEST="$1"
P="$PWD"
T="$P/$TEST"

set -e

if ! pushd . ; then
    DSTACK=""
    pushd ()
    {
        local dir="$(realpath "$1")"
        DSTACK="$DSTACK:///:$dir"
        cd "$dir"
    }
    popd ()
    {
        local dir="${DSTACK##*:///:}"
        DSTACK="${DSTACK%:///:*}"
        cd "$dir"
    }
else
    popd
fi

get_state ()
{
    echo 'State:'
    echo '--- main ---'
    pushd "$T"/etc
        hg status
        echo '---'
        hg diff --nodates
        echo '---'
        hg status -u | sed "s/^? //" | while read f ; do
            echo "$f:"
            cat "$f" | sed 's/^/    /'
            echo "END"
        done
    popd
    echo '--- default ---'
    hg status -R "$T"/var/etc-default
    echo '--- user ---'
    hg status -R "$T"/var/etc-user
    echo '--- END ---'
}
check_changed ()
{
    local before="$(get_state)"
    local ret=0
    "$@" || ret=$?
    local after="$(get_state)"
    if test "x$before" != "x$after" ; then
        echo 'State differs:'
        echo "$before" > "$T".before
        echo "$after" > "$T".after
        ndiff.py "$T".before "$T".after
        return 1
    fi
    return 0
}

if test -d "$T" ; then
    rm -r "$T"
    rm -f "$T".before
    rm -f "$T".after
fi

. "$P"/create-template.sh
pushd "$T"/etc
    . "$T"-prepare.sh || exit 2
    if . "$T".sh ; then
        rm -r "$T"
    else
        exit 1
    fi
popd
