#!/bin/sh
mkdir -p "$T/etc"
pushd "$T/etc"
    hg init .
    ln -s ../../../update-script.sh
    echo 'syntax: regexp' >> .hgignore
    echo '^update-script\.sh$' >> .hgignore
    hg add .hgignore
    hg commit .hgignore -m 'Initial commit'
    mkdir ../var
    hg clone . ../var/etc-default
    hg clone . ../var/etc-user
popd
